#!/bin/bash

SRC="$1"
T1=`tempfile`
T2=`tempfile`

if [ ! -d collections ] ; then
   echo "Run this from the health icons repo - should have a collections folder which will be removed"
   exit 1
fi

rm -rf collections
mkdir collections

find $SRC/public/icons/svg -name '*.svg' > $T1

awk -F / '
function basename(file) {
    sub(".*/", "", file)
    sub("\\..*","",file)
    return file
  }
{
 filename=$NF
 type=$(NF-2)
 collection=$(NF-1)
 newfile=basename(filename)"_"type".svg"

 print "copy "$0" to "newfile" in "collection
 system("mkdir -p collections/"collection"/svg/")
 system("cp "$0" collections/"collection"/svg/"newfile)
}
' < $T1

echo "[general]" > metadata.ini
echo -n "collections=" >>metadata.ini
echo collections/*| sed -e s'/collections\///g' -e s'/ /,/g' >>metadata.ini

echo "" >>metadata.ini
for collection in `ls collections/` ; do
    echo "["$collection"]" >>metadata.ini
    echo "name=Open Health Icons $collection " >>metadata.ini
    echo "author=resolvetosavelives" >>metadata.ini
    echo "email=contact@healthicons.org" >>metadata.ini
    echo "tags=SVGs" >>metadata.ini
    echo "description=Open Source Public Domain Health Icons ($collection)" >>metadata.ini
    echo "" >>metadata.ini
done


echo -n "Source version git log hash was " >version.txt
(cd $SRC ; git log -n1 --format=format:"%H") >> version.txt

python qgisify_svg.py ./collections/*/*/*.svg
