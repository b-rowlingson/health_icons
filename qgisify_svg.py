
""" 
add something like:

fill="param(fill) #FFF" stroke="param(outline) #000" stroke-width="param(outline-width) 1"

to path elements

"""

import argparse
from lxml import etree


def convert_svg(svg_path):
    tree = etree.parse(open(svg_path,"r"))
    root = tree.getroot()
    if "fill" in root.attrib:
        root.attrib.pop('fill')
    if root.attrib['width'] == "48":
        root.attrib['width'] = "512"
    if root.attrib['height'] == "48":
        root.attrib['height'] = "512"
    
    nsmap = root.nsmap
    paths = root.findall(".//path", nsmap)
    for path in paths:
        atts = path.attrib.keys()
        if "fill" in atts:
            print("Setting fill")
            if not "param(fill)" in path.attrib['fill']:
                path.attrib['fill'] = "param(fill) " + path.attrib['fill']
        if "stroke" in atts:
            print("Setting stroke")
            if not "param(outline)" in path.attrib['stroke']:
                path.attrib['stroke'] = "param(outline) " + path.attrib['stroke']
        if "stroke-width" in atts:
            print("Setting stroke-width")
            if not "param(outline-width)" in path.attrib['stroke-width']:
                path.attrib['stroke-width'] = "param(outline-width) " + path.attrib['stroke-width']
    tree.write(svg_path)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('svgs', nargs='+', type=str, help='svg files to convert')
    args = parser.parse_args()
    for one_svg in args.svgs:
        print(f"Converting {one_svg}")
        convert_svg(one_svg)
        
    
if __name__ == "__main__":
    main()

